import React from "react";
import Button from "@material-ui/core/Button";

function App() {
  return (
    <div className="App">
      <Button>Test</Button>
    </div>
  );
}

export default App;
